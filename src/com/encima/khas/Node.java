	/**
 * 
 */
package com.encima.khas;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;
import repast.simphony.visualization.editor.EdgeFinder;

import com.encima.dwc.DarwinCore;
import com.encima.dwc.Identification;
import com.encima.dwc.Image;
import com.encima.dwc.Occurrence;

/**
 * @author encima
 * 
 */
public class Node {

	double red, green, blue = 0;

	private String projection = "K-HAS/sensor network";

	protected Vector<DarwinCore> archives;

	private int hopsToDA = -1;
	int connectedNodes = 0;

	int nodeID;
	String transMethod;
	double transRange;
	double transRate; // in seconds

	int usedStorage = 0;
	int maxStorage;
	
	int daEmptyCount = 0;

	private SendState sendState;

	protected ContinuousSpace<Object> space;

	Properties transProps;
	String propsFile = "properties/rf.properties";

	Node receiver = null;

	int picCount = 3;

	int count = 0;

	Network network;
	
	String mode;

	protected Node getReceiver() {
		return receiver;
	}

	protected void setReceiver(Node receiver) {
		this.receiver = receiver;
	}

	public Node(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space, Network<Object> net, String mode) {
		super();
		archives = new Vector<DarwinCore>();
		transProps = new Properties();
		this.nodeID = nodeID;
		this.transMethod = transMethod;
		this.maxStorage = maxStorage;
		this.setSpace(space);
		this.network = net;
		this.mode = mode;

		// Load the correct properties file based on the transmission method
		// setting, using RF as default
		// Switch statement can't be used as Strings are only supported in 1.7
		// and repast has OpenGL issues in 1.7
		if (transMethod.toLowerCase().equals("rf"))
			propsFile = "properties/rf.properties";
		else if (transMethod.toLowerCase().equals("zigbee"))
			propsFile = "properties/zigbee.properties";
		else if (transMethod.toLowerCase().equals("wi-fi"))
			propsFile = "properties/rf.properties";

		try {
			transProps.load(new FileInputStream(propsFile));
			this.setTransRange(Double.parseDouble(transProps
					.getProperty("range")));
			this.setTransRate(Double.parseDouble(transProps.getProperty("rate")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void receive(DarwinCore recvd, int transTime) {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule()
				.getTickCount();
		recvd.addRoute(this);
//		recvd.addTransTime(transTime);
		if (transTime <= tick) {
			this.archives.add(recvd);
			if (this instanceof RoutingNode) {
				// On receipt of a set, images are processed, assuming 3 images
				// takes 90 seconds(ticks)
				RunEnvironment inst = RunEnvironment.getInstance();
				ScheduleParameters sp = null;
				Object[] obj = { recvd };
				String method = "";
				if((this.mode.equals("HKALL") || this.mode.equals("KHAS")) && recvd.getId() == null) {
					method = "highKnowledge";
					sp = ScheduleParameters.createOneTime(inst
							.getCurrentSchedule().getTickCount(),
							ScheduleParameters.FIRST_PRIORITY, 40);
				}else if((this.mode.equals("LKALL") || this.mode.equals("NKLK")) && recvd.getId() == null) {
					method = "lowKnowledge";
					sp = ScheduleParameters.createOneTime(inst
							.getCurrentSchedule().getTickCount(),
							ScheduleParameters.FIRST_PRIORITY, 5);
				}
				if(!method.equals("")) {
					RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, method, obj);
				}
			} else if (this instanceof DANode) {
//				System.out.println("Received at DA");
				if(recvd.getId() != null && recvd.getId().getSpeciesID().equalsIgnoreCase("EMPTY")) daEmptyCount++;
//				if(daEmptyCount == 98 || daEmptyCount == 99 || daEmptyCount == 100) recvd.getId().setSpeciesID("INTERESTING");
//				if(daEmptyCount == 100) daEmptyCount = 0;
				recvd.setEndTime(tick);
//				if(this.archives.size() > 13399) RunEnvironment.getInstance().endRun();
			}
		}
	}

	public void receiveBroadcast(Node requester) {
		this.setReceiver(requester);
		this.setHopsToDA(requester.getHopsToDA() + 1);	
		handleEdge(requester);
		Iterator iter = new ContinuousWithin(getSpace(), this, this.getTransRange()).query().iterator();
		// int count = 0;
//		System.out.println("Node: " + this.getNodeID()); 
		while (iter.hasNext()) {
			Node obj = (Node) iter.next();
//			System.out.println(obj.getNodeID() + ": " + obj.getHopsToDA()); 
			// Check if node does not have a link already. If it does, add new link only if it is fewer hops away
			if ((obj.getHopsToDA() == -1 && obj instanceof SensingNode) || (obj instanceof SensingNode && obj.getHopsToDA() != -1 && obj.getHopsToDA() + 1 > this.getHopsToDA() + 1)) {
				obj.receiveBroadcast(this);
				count++;
			}
		}
	}

	public void handleEdge(Node n) {
		List<RepastEdge> edges = new ArrayList<RepastEdge>();
		Iterator iter = network.getEdges(this).iterator();
		while(iter.hasNext()) {
			edges.add(((RepastEdge) iter.next()));
		}
		
		for (RepastEdge edge : edges) {
			network.removeEdge(edge);
		}
		RepastEdge re = new RepastEdge(this, n, false, 2);
		network.addEdge(re);
	}
	
	public void highKnowledge(DarwinCore received) {
		// Could be implemented to loop through the archives periodically.
		DarwinCore dwc = received;
		if(dwc.getId() == null) {
				Vector<Image> images = dwc.getIs();
				Calendar cal = Calendar.getInstance();
				if(dwc.getContent().equals("INTERESTING")) { 
					if (new Random().nextDouble() <= 0.82) {
						dwc.setId(new Identification(dwc.getOcc().getEventID(), nodeID, cal.getTime(), "INTERESTING"));
					} else {
						dwc.setId(new Identification(dwc.getOcc().getEventID(), nodeID, cal.getTime(), "EMPTY"));
					}
				}else{
					if (new Random().nextDouble() <= 0.97) {
						dwc.setId(new Identification(dwc.getOcc().getEventID(), nodeID, cal.getTime(), "EMPTY"));
					} else {
						dwc.setId(new Identification(dwc.getOcc().getEventID(), nodeID, cal.getTime(), "INTERESTING"));
					}
				}
		}	
	}
	
	public void lowKnowledge(DarwinCore received) {
		DarwinCore dwc = received;
		if (dwc.getId() == null) {
			Vector<Image> images = dwc.getIs();
			Calendar cal = Calendar.getInstance();
			if(dwc.getContent().equals("INTERESTING")) { 
				if (new Random().nextDouble() <= 0.10) {
					dwc.setId(new Identification(dwc.getOcc().getEventID(), nodeID, cal.getTime(), "INTERESTING"));
				}
			}
		}
	}
	
	public int getConnectedNodes() {
		return connectedNodes;
	}
	

	public DarwinCore genDWCRandom(int tick, int locID) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		dateFormat.format(cal.getTime());
		Occurrence occ = new Occurrence(tick, cal.getTime(), cal.getTime(),
				nodeID, "MOVINGIMAGE", nodeID);
		Vector<Image> isv = new Vector<Image>();
		for (int i = 0; i < 3; i++) {
			isv.add(new Image(i + 1, tick, "path" + i + 1 + ".jpg"));
		}
		// based on a 20% chance of an image containing an object, this is
					// not specific to location but taken across a whole set
		DarwinCore dwc;
		if (new Random().nextDouble() <= 0.207) {
			dwc = new DarwinCore(occ, null, isv, "INTERESTING");
		}else{
			dwc = new DarwinCore(occ, null, isv, "EMPTY");
		}
		dwc.addRoute(this);
		return dwc;
	}

	public Vector<DarwinCore> getArchives() {
		return archives;
	}

	public void setArchives(Vector<DarwinCore> archives) {
		this.archives = archives;
	}

	public void addArchives(DarwinCore archive) {
		this.archives.add(archive);
	}

	public int getNodeID() {
		return nodeID;
	}

	protected void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	protected String getTransMethod() {
		return transMethod;
	}

	protected void setTransMethod(String transMethod) {
		this.transMethod = transMethod;
	}

	protected double getTransRange() {
		return transRange;
	}

	protected void setTransRange(double transRange) {
		this.transRange = transRange;
	}

	protected double getTransRate() {
		return transRate;
	}

	protected void setTransRate(double transRate) {
		this.transRate = transRate;
	}

	protected int getUsedStorage() {
		return usedStorage;
	}

	protected void setUsedStorage(int usedStorage) {
		this.usedStorage = usedStorage;
	}

	protected int getMaxStorage() {
		return maxStorage;
	}

	protected void setMaxStorage(int maxStorage) {
		this.maxStorage = maxStorage;
	}

	protected Properties getTransProps() {
		return transProps;
	}

	protected void setTransProps(Properties transProps) {
		this.transProps = transProps;
	}

	public String getProjection() {
		return projection;
	}

	public void setProjection(String projection) {
		this.projection = projection;
	}

	SendState getSendState() {
		return sendState;
	}

	void setSendState(SendState sendState) {
		this.sendState = sendState;
	}

	protected void setHopsToDA(int hopsToDA) {
		this.hopsToDA = hopsToDA;
	}

	protected int getHopsToDA() {
		return hopsToDA;
	}

	public void setConnectedNodes(int connectedNodes) {
		this.connectedNodes = connectedNodes;
	}

	// Simulation Methods
	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	ContinuousSpace<Object> getSpace() {
		return space;
	}

	void setSpace(ContinuousSpace<Object> space) {
		this.space = space;
	}

	// Colour Methods
	public double getRed() {
		if (this instanceof DANode)
			return 255;
		else
			return 0;
	}

	public void setRed(double red) {
		this.red = red;
	}

	public double getGreen() {
		if (this instanceof SensingNode)
			return 255;
		else
			return 0;
	}

	public void setGreen(double green) {
		this.green = green;
	}

	public double getBlue() {
		if (this instanceof RoutingNode)
			return 255;
		else
			return 0;
	}

	public void setBlue(double blue) {
		this.blue = blue;
	}
}
