/**
 * 
 */
package com.encima.khas;

import java.io.File;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;

import com.encima.dwc.DarwinCore;
import com.encima.dwc.Identification;
import com.encima.dwc.Image;

/**
 * @author encima
 * 
 */
public class RoutingNode extends Node {

	Vector<NdPoint> daLocs;
	private double processing = -1;
	Vector<SensingNode> conns;
	Vector<RoutingNode> neighbourDPs;
	Vector<SensingNode> neighbourDCs;
	Vector<DarwinCore> inter, empty;
	
	/**
	 * @param nodeID
	 * @param transMethod
	 * @param maxStorage
	 * @param space
	 */
	public RoutingNode(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space, Network<Object> net, String mode) {
		super(nodeID, transMethod, maxStorage, space, net, mode);
		this.daLocs = daLocs;
		conns = new Vector<SensingNode>();
		neighbourDPs = new Vector<RoutingNode>();
		neighbourDCs = new Vector<SensingNode>();
		empty = new Vector<DarwinCore>();
		inter = new Vector<DarwinCore>();
	}

	@ScheduledMethod(start = 10, interval = 60, priority = 1.7976931348623157E308d)
	public void sendSet() {
			if (this.getSendState() != null && this.getReceiver() != null) {
				int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
				// On preparation of sending the file, calculate the send time
				// from the start
				DarwinCore toSend = (DarwinCore) this.getSendState().getFile();
				if (this.getSendState().getTransTime() == -1) {
					double tts =(toSend.getSize() / this.getReceiver().getTransRate());
//					System.out.println((int) tts);
					int transTime = tick + (int) tts;
					this.getSendState().setTransTime(transTime);
				} else if (this.getSendState().getTransTime() <= tick) {
					this.getReceiver().receive(toSend,
							this.getSendState().getTransTime());
					archives.remove(toSend);
					this.setSendState(null);
				}
			} else if (this.archives.size() > 0) {
				int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
				if(this.mode.equals("NKALL")) {
					DarwinCore toSend = null;
					int maxDiff = 0;
					for(DarwinCore d: this.getArchives()) {
						if((tick - d.getOcc().getEventID()) > maxDiff) {
							toSend = d;
							maxDiff = tick-d.getOcc().getEventID();
						}
					}
					if(toSend != null)
						this.setSendState(new SendState(toSend, -1));
					if(this.getSendState() == null)
						this.setSendState(new SendState(this.archives.firstElement(), -1));
				}else{
					DarwinCore emptyArchive = null;
					DarwinCore intArchive = null;
					for(DarwinCore dwc: this.archives) {
						if(dwc.getId() != null && dwc.getId().getSpeciesID().equals("INTERESTING") && intArchive == null) {
							intArchive = dwc;
						}else if(dwc.getId() != null && dwc.getId().getSpeciesID().equals("INTERESTING") && intArchive.getOcc().getEventID() < dwc.getOcc().getEventID()) {
							intArchive = dwc;
						}
					}
					if(intArchive != null)
						this.setSendState(new SendState(intArchive, -1));
					else
						this.setSendState(new SendState(this.archives.firstElement(), -1));
				}
				this.sendSet();
			}
	}
	
	public void link(Node req) {
		handleEdge(req);
		this.setReceiver(req);
		this.setHopsToDA(req.getHopsToDA() + 1);
		Iterator iter = new ContinuousWithin(space, this, this.getTransRange()).query().iterator();
//		Get all DC and DP nodes in transmission range
		while (iter.hasNext()) {
			Object n = iter.next();
			if(n instanceof RoutingNode && !neighbourDPs.contains(n)) {
				RoutingNode dp = (RoutingNode) n;
				neighbourDPs.add(dp);
			}else if(n instanceof SensingNode && !neighbourDCs.contains(n)) {
				neighbourDCs.add((SensingNode) n);
			}
		}
		if(neighbourDPs.size() > 0) {
			int maxConns = (neighbourDCs.size()/neighbourDPs.size()) + 1;
			Iterator<SensingNode> dcIter = neighbourDCs.iterator();
			System.out.println("Max connections allowed for Node " + this.getNodeID() + ": " + maxConns);
			while(dcIter.hasNext()) {
				SensingNode dc = dcIter.next();
				if(this.calculateConnectedNodes() < maxConns) {
					if(dc.getReceiver() == null || (dc.getReceiver() != null && dc.getReceiver() instanceof SensingNode)) {
						dc.receiveBroadcast(this);
						dcIter.remove();
//						System.out.println(this.getNodeID() + " connecting to: " + dc.getNodeID() + ". Active Connections: " + this.calculateConnectedNodes() + " of " + maxConns);
					}
				}
			}
		}else{
			Iterator<SensingNode> dcIter = neighbourDCs.iterator();
			while(dcIter.hasNext()) {
				SensingNode dc  = dcIter.next();
				if(dc.getReceiver() == null || (dc.getReceiver() != null && dc.getReceiver() instanceof SensingNode)) {
					dc.receiveBroadcast(this);
					dcIter.remove();
//					System.out.println(this.getNodeID() + " connecting to: " + dc.getNodeID());
				}
			}
		}
//		TODO - Add delegation to neighouring DPs when this node is full
		System.out.println("------");
	}
	
//	@ScheduledMethod(start = 0, interval = 250000)
	public int calculateConnectedNodes() {
		this.setConnectedNodes(0);
		int cn = 0;
//		Get edges connected to node
		Iterator<RepastEdge> itera = this.getNetwork().getEdges(this).iterator();
		while(itera.hasNext()) {
			RepastEdge re = itera.next();
			Node s = (Node) re.getSource();
			Node t = (Node) re.getTarget();
			if(s.equals(this) || t.equals(this)) {
				cn++;
				this.setConnectedNodes(cn);
			}
				
		}
		return this.getConnectedNodes();
	}

	boolean isProcessing() {
		if (this.processing == -1)
			return false;
		else
			return true;
	}

	void setProcessing(double tick) {
		this.processing = processing;
	}

}
