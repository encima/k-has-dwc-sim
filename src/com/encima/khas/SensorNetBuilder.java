/**
 * 
 */
package com.encima.khas;

import java.io.File;
import java.util.Vector;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.graph.Network;

import com.encima.dwc.DarwinCore;
import com.encima.utils.FileUtils;
import com.encima.utils.OutputRecorder;

/**
 * @author encima
 * 
 */
public class SensorNetBuilder extends DefaultContext<Object> implements
		ContextBuilder<Object> {
	ContinuousSpace<Object> space;

	@Override
	public Context<Object> build(Context<Object> context) {

		context.setId("K-HAS");

		// Geography<Object> geography =
		// GeographyFactoryFinder.createGeographyFactory(null)
		// .createGeography("Geography", context, new
		// GeographyParameters<Object>());

		NetworkBuilder<Object> netbuild = new NetworkBuilder<Object>(
				"sensor network", context, true);
		Network<Object> net = netbuild.buildNetwork();

		Parameters params = RunEnvironment.getInstance().getParameters();
		int height = (Integer) params.getValue("height");
		int width = (Integer) params.getValue("width");
		int daNodeCount = (Integer) params.getValue("daNodeCount");
		int dpNodeCount = (Integer) params.getValue("dpNodeCount");
		int dcNodeCount = (Integer) params.getValue("dcNodeCount");
		int dcHK = (Integer) params.getValue("dcHK");
		String mode = (String) params.getValue("mode");
		int csv = (Integer) params.getValue("csv");
		int tickStop = (Integer) params.getValue("tickStop");
		
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder
				.createContinuousSpaceFactory(null);
		space = spaceFactory.createContinuousSpace("space", context,
				new RandomCartesianAdder<Object>(),
				new repast.simphony.space.continuous.StrictBorders(), width,
				height);

		// Initialise log file and write start time
		OutputRecorder recorder = new OutputRecorder(context, mode);

		Vector<String> images = null;
		if (csv == 1)
			images = FileUtils.CSVReader(new File("properties/lot5.csv"));

		int i = 0;
		Vector<DarwinCore> dwcs = null;
		if(mode.equals("KHAS") && dcHK != -1) {
			for(; i < dcHK; i++) {
				System.out.println("*Adding HK DC node");
				SensingNode n = new SensingNode(i, "rf", 2097, space, net, dwcs, "HKALL");
				context.add(n);
//				dcNodeCount--;
			}
		}
		for (; i < dcNodeCount; i++) {
			if (csv == 1)
				dwcs = FileUtils.readArchives(images, i);
//				System.out.println("Adding DC node");
				SensingNode n = new SensingNode(i, "rf", 2097, space, net, dwcs, mode);
				context.add(n);
		}

		for (; i < dpNodeCount + dcNodeCount; i++) {
			RoutingNode dpn = new RoutingNode(i, "rf", 2097, space, net, mode);
			context.add(dpn);
		}

		for (; i < daNodeCount + dcNodeCount + dpNodeCount; i++) {
			DANode dan = new DANode(i, "wi-fi", 2097, space, net, recorder, mode);
			context.add(dan);
		}
		
		if(tickStop > -1) {
//			End after 6 month deployment
			RunEnvironment.getInstance().endAt(tickStop);
		}

		// Schedule custom output to log file at the end of the simulation
		ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
		ScheduleParameters schedParams = ScheduleParameters.createAtEnd(0);
		schedule.schedule(schedParams, recorder, "finalise");

		System.out.println("***********Initialised Network***********");
		return context;
	}

}
