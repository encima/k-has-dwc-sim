/**
 * 
 */
package com.encima.khas;

import java.util.Iterator;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;

import com.encima.utils.OutputRecorder;

/**
 * @author encima
 * 
 */
public class DANode extends Node {

	/**
	 * @param nodeID
	 * @param transMethod
	 * @param maxStorage
	 * @param space
	 */

	OutputRecorder record;

	public DANode(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space, Network<Object> net,
			OutputRecorder recorder, String mode) {

		super(nodeID, transMethod, maxStorage, space, net, mode);

		this.record = recorder;

		RunEnvironment inst = RunEnvironment.getInstance();
		ScheduleParameters sp = ScheduleParameters.createOneTime(1);
		RunEnvironment.getInstance().getCurrentSchedule()
				.schedule(sp, this, "linkDPNodes");
	}

	// Searches for Data Processing nodes, adding them as receivers, runs every week.
//	@ScheduledMethod(start = 0, interval = 100000)
	public void linkDPNodes() {
		// Create start of log file(used because this runs at tick 0)
//		record.create();
		System.out.println("Initiating Network setup");
		Parameters params = RunEnvironment.getInstance().getParameters();
		int dpNodeCount = (Integer) params.getValue("dpNodeCount");
		Iterator iter = new ContinuousWithin(space, this, this.getTransRange()).query().iterator();
		int count = 0;
		while (iter.hasNext()) {
			Object n = iter.next();
			if (n instanceof RoutingNode) {
				RoutingNode dp = (RoutingNode) n;
				dp.link(this);
				count++;
			} else if(this.mode.equalsIgnoreCase("DUMB") && n instanceof SensingNode) {
				SensingNode dc = (SensingNode) n;
				dc.receiveBroadcast(this);
				count++;
			}
		}
		if (count == 0) {
			System.out.println("No neighbouring DP nodes found, ending simulation");
			RunEnvironment.getInstance().endRun();
		} else {
			System.out.println("Network Setup Complete, Connected DP Nodes: " + count + " of " + dpNodeCount);
		}
	}
}
