/**
 * 
 */
package com.encima.khas;

import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.graph.Network;

import com.encima.dwc.DarwinCore;

/**
 * @author encima
 * 
 */
public class SensingNode extends Node {

	Vector<NdPoint> dpLocs;
	boolean sending = false;
	Vector<DarwinCore> images = null;
	/**
	 * @param nodeID
	 * @param transMethod
	 * @param maxStorage
	 * @param space
	 */
	public SensingNode(int nodeID, String transMethod, int maxStorage,
			ContinuousSpace<Object> space, Network<Object> net,
			Vector<DarwinCore> images, String mode) {
		super(nodeID, transMethod, maxStorage, space, net, mode);
		this.images = images;
		// RunEnvironment inst = RunEnvironment.getInstance();
		// ScheduleParameters sp = ScheduleParameters.createOneTime(10);
		// RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this,
		// "linkNodes");
	}

	@ScheduledMethod (start = 10, interval = 1000, priority = 1.7976931348623157E308d)
	public void takeImage() {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		// Random Image Capture
		if (images == null) {
//			if (Math.random() * 10 <= 1.1) {
			if(new Random().nextDouble() * 10 >= 8.61561) {
				DarwinCore dwc = genDWCRandom(tick, nodeID);
				//TODO: Add processing time to this
				RunEnvironment inst = RunEnvironment.getInstance();
				ScheduleParameters sp = null;
				Object[] obj = { dwc };
				String method = "";
				if(this.mode.equals("HKALL")) {
					method = "highKnowledge";
					sp = ScheduleParameters.createOneTime(inst
							.getCurrentSchedule().getTickCount(),
							ScheduleParameters.FIRST_PRIORITY, 90);
				}else if(this.mode.equals("LKALL") || this.mode.equals("KHAS")) {
					method = "lowKnowledge";
					sp = ScheduleParameters.createOneTime(inst
							.getCurrentSchedule().getTickCount(),
							ScheduleParameters.FIRST_PRIORITY, 5);
				}
				if(!method.equals("")) {
					RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, method, obj);
				}
				this.archives.add(dwc);
				sendImage();
			}
		} else if (images.size() > 0) {
			// Fixed Image Capture
			Iterator<DarwinCore> iter = images.iterator();
			while (iter.hasNext()) {
				DarwinCore d = iter.next();
				// Add archive if it matches the tick (due to memory hogging,
				// some may be missed, so add if tick is higher)
				if (d.getOcc().getEventID() == tick
						|| tick > d.getOcc().getEventID()) {
					this.archives.add(d);
					// Remove object to speed up future iterations
					iter.remove();
				}
			}
		} else {
			// Fixed simulation has ended, switching to random occurrences
			System.out.println("Fixed simulation has ended, switching to random occurrences");
//			images = null;
		}
	}

	@ScheduledMethod(start = 10, interval = 60, priority = 1.7976931348623157E308d)
	public void sendImage() {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		if (this.getSendState() != null && this.getReceiver() != null) {
			// On preparation of sending the file, calculate the send time from
			// the start
			DarwinCore toSend = (DarwinCore) this.getSendState().getFile();
//			System.out.println(this.getSendState().getTransTime());
			if (this.getSendState().getTransTime() == -1) {
				double tts =(toSend.getSize() / this.getReceiver().getTransRate());
//				System.out.println((int) tts);
				int transTime = tick + (int) tts;
				this.getSendState().setTransTime(transTime);
			} else if (tick >= this.getSendState().getTransTime()) {
				this.getReceiver().receive(toSend, this.getSendState().getTransTime());
//				System.out.println("Data Sent from DC" + this.getNodeID() + " to " + this.getReceiver().getNodeID());
				archives.remove(toSend);
				this.setSendState(null);
			}
		} else if (this.archives.size() > 1) {
			if(this.mode.equals("NKALL") || this.mode.equals("NKLK")) {
				DarwinCore toSend = null;
				int maxDiff = 0;
				for(DarwinCore d: this.getArchives()) {
					if((tick - d.getOcc().getEventID()) > maxDiff) {
						toSend = d;
						maxDiff = tick-d.getOcc().getEventID();
					}
				}
				if(toSend != null)
					this.setSendState(new SendState(toSend, -1));
				if(this.getSendState() == null)
					this.setSendState(new SendState(this.archives.firstElement(), -1));
			} else {
				DarwinCore emptyArchive = null;
				DarwinCore intArchive = null;
				for(DarwinCore dwc: this.archives) {
					if(dwc.getId() != null && dwc.getId().getSpeciesID().equals("INTERESTING") && intArchive == null) {
						intArchive = dwc;
					}else if(dwc.getId() != null && dwc.getId().getSpeciesID().equals("INTERESTING") && intArchive.getOcc().getEventID() > dwc.getOcc().getEventID()) {
						intArchive = dwc;
					}
				}
				if(intArchive != null)
					this.setSendState(new SendState(intArchive, -1));
				else
					this.setSendState(new SendState(this.archives.firstElement(), -1));
			}
			this.sendImage();
		}
	}

	public boolean isSending() {
		return sending;
	}

	public void setSending(boolean sending) {
		this.sending = sending;
	}
	
	public void link(Node n) {
		handleEdge(n);
		this.setReceiver(n);
		
	}

	// Repast seems to leak memory like a mother, 2MB for every few thousand
	// ticks
	// @ScheduledMethod (start = 1000000, interval = 1000000, priority =
	// ScheduleParameters.LAST_PRIORITY)
	// public void gc() {
	// System.gc();
	// }
}
