package com.encima.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.encima.dwc.DarwinCore;
import com.encima.dwc.Image;
import com.encima.dwc.Occurrence;

public class FileUtils {

	public FileUtils() {
		// TODO Auto-generated constructor stub
	}

	public static Vector<String> CSVReader(File csv) {
		System.out.println("Reading images from: " + csv);
		BufferedReader csvReader;
		try {
			csvReader = new BufferedReader(new FileReader(csv));
			String row;
			Vector<String> images = new Vector<String>();
			int count = 0;
			while ((row = csvReader.readLine()) != null) {
				images.add(row);
			}
			csvReader.close();
			return images;
		} catch (Exception e) {
			System.out.println("Error: " + e);
			return null;
		}
	}

	public static Vector<DarwinCore> readArchives(Vector<String> images,
			int nodeID) {
		Vector<DarwinCore> dwcs = new Vector<DarwinCore>();
		for (int i = 0; i < images.size() - 2; i += 2) {
			if (images.get(i + 1) != null && images.get(i + 2) != null) {
				String[] img1 = formatLine(images.get(i));
				String[] img2 = formatLine(images.get(i + 1));
				String[] img3 = formatLine(images.get(i + 2));
				Date date = null;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(img1[1].toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				int tick = Integer.parseInt(img1[4].toString());
				int node = Integer.parseInt(img1[0].toString());
				if (node == nodeID) {

					int size1 = Integer.parseInt(img1[3].toString());
					int size2 = Integer.parseInt(img2[3].toString());
					int size3 = Integer.parseInt(img3[3].toString());

					String path1 = img1[2].toString();
					String path2 = img2[2].toString();
					String path3 = img3[2].toString();

					// int id1 =
					// Integer.parseInt(path1.substring(path1.length()-12).replaceAll("\\D+",""));
					// int id2 =
					// Integer.parseInt(path2.substring(path1.length()-12).replaceAll("\\D+",""));
					// int id3 =
					// Integer.parseInt(path3.substring(path1.length()-12).replaceAll("\\D+",""));

					int id1 = node, id2 = node, id3 = node;

					Occurrence occ = new Occurrence(tick, date, date, node,
							"MOVINGIMAGE", node);
					Vector<Image> imgs = new Vector<Image>();
					imgs.add(new Image(id1, tick, path1, size1));
					imgs.add(new Image(id2, tick, path2, size2));
					imgs.add(new Image(id3, tick, path3, size3));

					dwcs.add(new DarwinCore(occ, null, imgs, null));
				}
			}
		}
		return dwcs;
	}

	public static String[] formatLine(String line) {
		String[] rowElements = line.split("~");
		for (int i = 0; i < rowElements.length; i++) {
			rowElements[i] = rowElements[i].replaceAll("\"", "");
		}
		return rowElements;
	}

}
