package com.encima.utils;

import java.awt.Color;

import repast.simphony.space.graph.RepastEdge;
import repast.simphony.visualizationOGL2D.DefaultEdgeStyleOGL2D;

import com.encima.khas.DANode;
import com.encima.khas.SensingNode;
import com.encima.khas.RoutingNode;
import com.encima.khas.Node;

public class ColouredEdgeStyle extends DefaultEdgeStyleOGL2D {

	private String label;

	double red, green, blue = 128;

	public ColouredEdgeStyle() {
		super();
	}

	public Color getColor(RepastEdge obj) {
		if (obj.getSource() instanceof DANode
				|| obj.getSource() instanceof DANode)
			return Color.YELLOW;
		else if ((obj.getSource() instanceof RoutingNode && !(obj.getTarget() instanceof DANode))
				|| (obj.getTarget() instanceof RoutingNode && !(obj.getSource() instanceof DANode)))
			return Color.PINK;
		else
			return Color.CYAN;
	}

	public int getLineWidth(RepastEdge obj) {
		if (obj.getSource() instanceof DANode)
			return 5;
		else if (obj.getSource() instanceof RoutingNode)
			return 3;
		else
			return 1;
	}

	public String getLabel(Object obj) {
		Node n = (Node) obj;
		return String.valueOf(n.getArchives().size());
	}

	public String getLabel() {
		return label;
	}

}
