package com.encima.utils;

import repast.simphony.space.graph.RepastEdge;

public class LabelledEdge extends RepastEdge<Object> {

	private String label;

	int red, green, blue = 128;

	public LabelledEdge(Object source, Object target, boolean directed,
			double weight, String label) {
		super(source, target, directed, weight);
		this.label = label;
	}

}
