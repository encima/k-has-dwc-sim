package com.encima.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;

import repast.simphony.context.Context;

import com.encima.dwc.DarwinCore;
import com.encima.khas.DANode;
import com.encima.khas.Node;

public class OutputRecorder {

	Context context;
	SimpleDateFormat sdf = new SimpleDateFormat("ddmmyyyy_HHmmss");
	Path file;
	long start = 0;

	public OutputRecorder(Context context, String mode) {
		this.context = context;
		file = Paths.get("output/testing/zigbee/LKALL/" + mode +"_RUN"
				+ sdf.format(Calendar.getInstance().getTime()) + ".csv");
		try {
			Files.createFile(file);
			Files.write(file,
					"DWC_ID, Processed Contents, Actual Contents, Route, Total_Trans_Time, Trans_Time_Hours \n"
							.getBytes(), StandardOpenOption.APPEND);
			System.out.println("Log File created at " + file.toString());
		} catch (IOException e1) {
			System.out.println("Log file creation failed, probably because it already exists");
		}
	}

	public void create() {
		try {
			start = System.currentTimeMillis();
			String startTime = "Start: " + start + "\n";
			Files.write(file, startTime.getBytes(), StandardOpenOption.APPEND);
			System.out.println("Logging Started: " + start);
		} catch (IOException e1) {
			System.out.println("Log file creation failed, probably because it already exists");
		}
	}

	public void setupTime() {
		if (file != null) {
			long setup = System.currentTimeMillis();
			String setupComp = "Setup Complete: " + (setup) + "\n";
			String setupTime = "Setup Time: " + (setup - start) + "\n";
			try {
				Files.write(file, setupComp.getBytes(),
						StandardOpenOption.APPEND);
				Files.write(file, setupTime.getBytes(),
						StandardOpenOption.APPEND);
				Files.write(file,
						"DWC_ID, Processed Contents, Actual Contents, Route, Total_Trans_Time, Trans_Time_Hours \n"
								.getBytes(), StandardOpenOption.APPEND);
				System.out.println(setupTime);
			} catch (IOException e) {
				System.out.println("Error appending network setup time");
				e.printStackTrace();
			}
		}
	}

	public void finalise() {
		System.out.println("*****Simulation Ended********");
		Iterator<DANode> daNodes = context.getObjects(DANode.class).iterator();
		while (daNodes.hasNext()) {
			System.out.println("Looping through nodes");
			DANode dan = daNodes.next();
			Vector<DarwinCore> dwc = dan.getArchives();
			for (DarwinCore d : dwc) {
				Iterator route = d.route.values().iterator();
				StringBuilder line = new StringBuilder();
				line.append(d.getOcc().getEventID());
				line.append(", ");
				if (d.getId() != null)
					line.append(d.getId().getSpeciesID());
				line.append(", ");
				line.append(d.getContent() + ", ");
				while (route.hasNext()) {
					Node n = (Node) route.next();
					if (route.hasNext())
						line.append(n.getNodeID() + ":");
					else
						line.append(n.getNodeID() + ", ");
				}
				long dur = d.getEndTime() - d.getOcc().getEventID();
				line.append(dur + ", ");
				line.append((dur / 60) / 60);
				line.append("\n"); 
				line.append("\n"); 
				try {
					Files.write(file, line.toString().getBytes(), StandardOpenOption.APPEND);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
//			try {
//				Files.write(file, "---------------------------\n".toString()
//						.getBytes(), StandardOpenOption.APPEND);
//				String endTime = "End: " + System.currentTimeMillis() + "\n";
//				Files.write(file, endTime.getBytes(), StandardOpenOption.APPEND);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
		System.out.println("Output logged to " + file.toAbsolutePath());
	}

}
