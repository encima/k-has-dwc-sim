import os, sys, csv

source = 'NKLK_UNRES'
files = os.listdir(source)
for f in files:
	if f.endswith('.csv'):
		lines = []
		with open(source + '/' + f, 'rUb') as csvfile:
			csvreader = csv.reader(csvfile, delimiter=',')
			for row in csvreader:
				if len(row) == 6:
					lines.append(row)
				# else:
					# print 'Ignoring short row'
			# lines=lines[:-3]
		with open(source + '/' + f, 'w+') as csvfile:
			cWriter = csv.writer(csvfile, delimiter=',')
			for line in lines:
				if len(line) > 0 and len(line) == 6:
			    		cWriter.writerow(line)
