# csvdata<-read.csv(file.choose(), header=TRUE, stringsAsFactors=FALSE)
setwd('~/Development/java/repast/K-HAS/output/testing/HKALL')
files <- list.files(pattern = "\\.(csv|CSV)$")
csvdata<-read.csv(files[1], header = TRUE, stringsAsFactors=FALSE) 
for(i in 2:length(files)) {
	temp <- read.csv(files[i], header = TRUE, stringsAsFactors=FALSE) 
	csvdata<-rbind(csvdata, temp)
} 
print(length(files))

# create truepositive, falsepositive, true negative and falsenegative data frames
int_data<-subset(csvdata, csvdata$Actual.Contents==" INTERESTING")
tp_data<-subset(int_data, int_data$Processed.Contents==" INTERESTING")
fn_data<-subset(int_data, int_data$Processed.Contents==" EMPTY")
if(nrow(fn_data) == 0) {
  print('fn_data is empty, filling...')
  fn_data<-subset(int_data, !int_data$Processed.Contents==" INTERESTING")
}

empty_data<-subset(csvdata, csvdata$Actual.Contents==" EMPTY")
tn_data<-subset(empty_data, empty_data$Processed.Contents==" EMPTY")
fp_data<-subset(empty_data, empty_data$Processed.Contents==" INTERESTING")

getStats <- function(dataset) {
  data_mean <- mean(dataset$Total_Trans_Time)
  data_median <- median(dataset$Total_Trans_Time)
  data_sd <- sd(dataset$Total_Trans_Time)
  data_se <- (sd(dataset$Total_Trans_Time)/(sqrt(100)))
    #length(files))))
  return(list(mean=data_mean, median=data_median, sd=data_sd, se=data_se))
}

print(getwd())
print('STATS')
print('TOTAL:')
total_stats <- getStats(csvdata)
print(total_stats)
print('*******')
print('INTERESTING:')
int_stats <- getStats(int_data)
int_stats <- c(int_stats, percent=((nrow(int_data)/nrow(csvdata))*100))
print(int_stats)
print('*******')
print('*TP:')
tp_stats <- getStats(tp_data)
tp_stats <- c(tp_stats, percent=((nrow(tp_data)/nrow(csvdata))*100))
print(tp_stats)
print('*******')
print('*FN:')
fn_stats <- getStats(fn_data)
fn_stats <-c(fn_stats, percent=((nrow(fn_data)/nrow(csvdata))*100))
print(fn_stats)
print('*******')
empty_stats <- getStats(empty_data)
empty_stats <- c(empty_stats, percent=((nrow(empty_data)/nrow(csvdata))*100))
print(empty_stats)
print('*******')
tn_stats <- getStats(tn_data)
tn_stats <- c(tn_stats, percent=((nrow(tn_data)/nrow(csvdata))*100))
print(tn_stats)
print('*******')
fp_stats <- getStats(fp_data)
fp_stats <- c(fp_stats, percent=((nrow(fp_data)/nrow(csvdata))*100))
print(fp_stats)

df <- data.frame(total=total_stats, int=int_stats, tp=tp_stats, fn=fn_stats, empty=empty_stats, tn=tn_stats, fp=fp_stats)
write.csv(df, file='stats.txt')
print('output saved')