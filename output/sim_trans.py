import os, sys, csv
from collections import defaultdict
import numpy

source = 'KHAS/KHAS4'
mode = 'KHAS'

def process_csv(filename, mode):
	with open(filename, 'rb') as csvfile:
		results = defaultdict(int)
		csvreader = csv.reader(csvfile, delimiter=',')
		next(csvreader, None)
		for row in csvreader:
			if(len(row) == 6):
				results['td'] += int(row[5])
				results['to'] += 1
				if 'NKALL' not in mode:
					if 'INTERESTING' in str(row[2]):
						results['ti'] += 1
						results['ind'] += int(row[5])
						if 'INTERESTING' in str(row[1]):
							results['tp'] += 1
							results['tpd'] += int(row[5])
						elif 'EMPTY' in str(row[1]):
							results['fn'] += 1
							results['fnd'] += int(row[5])
						else:
							results['ui'] += 1
					elif 'EMPTY' in str(row[2]):
						results['te'] += 1
						results['emd'] += int(row[5])
						if 'EMPTY' in str(row[1]):
							results['tn'] += 1
							results['tnd'] += int(row[5])
						elif 'INTERESTING' in str(row[1]):
							results['fp'] += 1
							results['fpd'] += int(row[5])
						else:
							results['ue'] += 1
	return results

if(len(sys.argv)>1):
	source = str(sys.argv[1])
if(os.path.isdir(source)):
	print source
	files = os.listdir(source)
	mode = 'NKALL'
	totals = defaultdict(int)
	count_csv = total_avg_int = total_avg_empty = total_avg_dur = 0
	for f in files:
		if f.endswith('csv'):
			mode = f.split('/')[-1].split('_')[0]
			count_csv += 1
			path = source + '/' + f
			results = process_csv(path, mode)
			for res in results:
				totals[res] += results[res]
			# print '--------'
			# print 'Avg. Total Duration %f' % (results['td']/results['to'])
			# if results['tp']:
			# 	print 'Avg. True Positive Duration %d' % (results['tpd']/results['tp'])
			# 	print 'Percent True Positives %f' % ((float(results['tp'])/float(results['ti'])) *100)
			# if results['fp']:
			# 	print 'Avg. False Positive Duration %d' % (results['fpd']/results['fp'])
			# if results['tn']:
			# 	print 'Avg. True Negative Duration %d' % (results['tnd']/results['tn'])
			# if results['fn']:
			# 	print 'Avg. False Negative Duration %d' % (results['fnd']/results['fn'])
			# print '--------'
	print 'Total Observations: %d' % totals['to']
	print 'Avg. Total Duration %f' % (totals['td']/totals['to'])
	if totals['ti']:
		print 'Avg. Interesting Duration %f' % (totals['ind']/totals['ti'])
		print 'Percent Interesting %f' % ((float(totals['ti'])/float(totals['to'])) *100)
	if totals['tp'] and totals['fn']:
		print 'Avg. True Positive Duration %d' % (totals['tpd']/totals['tp'])
		print 'Percent True Positives %f' % ((float(totals['tp'])/float(totals['ti'])) *100)
		print 'Avg. False Negative Duration %d' % (totals['fnd']/totals['fn'])
		print 'Percent False Negative %f' % ((float(totals['fn'])/float(totals['ti'])) *100)
	if totals['te']:
		print 'Avg. Empty Duration %f' % (totals['emd']/totals['te'])
		print 'Percent Interesting %f' % ((float(totals['te'])/float(totals['to'])) *100)
elif(os.path.isfile(source)):
	mode = source.split('/')[-1].split('_')[0]
	results = process_csv(source, mode)
	print '--------'
	print 'Total Observations: %d' % results['to']
	print 'Avg. Total Duration %f' % (results['td']/results['to'])
	if results['ti']:
		print 'Avg. Interesting Duration %f' % (results['ind']/results['ti'])
		print 'Percent Interesting %f' % ((float(results['ti'])/float(results['to'])) *100)
	if results['tp'] and results['fn']:
		print 'Avg. True Positive Duration %d' % (results['tpd']/results['tp'])
		print 'Percent True Positives %f' % ((float(results['tp'])/float(results['ti'])) *100)
		print 'Avg. False Negative Duration %d' % (results['fnd']/results['fn'])
		print 'Percent False Negative %f' % ((float(results['fn'])/float(results['ti'])) *100)
	print '--------'